import { Component, OnInit,EventEmitter,Output, Input } from '@angular/core';
import {PostsService} from "../../services/posts.service";
import {Post} from "../../models/Post";

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {
//Properties
  // defining our Output as 'newPost', setting to an EventEmitter, which will have the
  //of Post and assigning it as a new EventEmitter.
  @Output() newPost: EventEmitter<Post> = new EventEmitter();
  @Output() updatedPost: EventEmitter<Post> = new EventEmitter();
  //defined our Input as a property 'currentPost' and set it as a Post data-type
  @Input() currentPost:Post;
  @Input() isEdit: boolean;
  //injection service as a dependency
  constructor(private postsService: PostsService) {
  }

  ngOnInit(): void {
  }

  addPost(title, body) {
    if (!title || !body) {
      alert('Please enter a post')
    } else {
      // console.log(title, body)

      this.postsService.savePost({title,body} as Post).subscribe(post =>{
          // console.log(posts);
          this.newPost.emit(post)
        }
      )
    }
  }

  updatePost(){
    // console.log("Updating post ....");
    this.postsService.updatePost(this.currentPost).subscribe(post => {
      console.log(post);
      this.isEdit = false;
      this.updatedPost.emit(post)
      })
  }
}
