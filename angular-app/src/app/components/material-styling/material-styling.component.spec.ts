import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialStylingComponent } from './material-styling.component';

describe('MaterialStylingComponent', () => {
  let component: MaterialStylingComponent;
  let fixture: ComponentFixture<MaterialStylingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialStylingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialStylingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
