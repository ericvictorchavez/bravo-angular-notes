import { Component, OnInit } from '@angular/core';
import {Member} from "../../models/Member";
import {hasI18nAttrs} from "@angular/compiler/src/render3/view/i18n/util";

@Component({
  selector: 'app-vip',
  templateUrl: './vip.component.html',
  styleUrls: ['./vip.component.css']
})
// @Component({
//   selector: 'app-vip',
//   template:`<h1>Hello </h1>`,
//   styleUrls: ['./vip.component.css']
// })
export class VipComponent implements OnInit {

  member: Member[];

 loadingMembers: boolean =false;



  constructor()

{
      this.member = [
        {
        firstName: 'Eric',
        lastName: 'Chavez',
        username: 'Barvictos',
        memberNo: 23
      },
      {
        firstName: 'Bob',
        lastName: 'Builder',
        username: 'WeFix',
        memberNo: 45
      },
       {
        firstName: 'Jake',
        lastName: 'White',
        username: 'Angles',
        memberNo: 43
      },
      {
        firstName: 'Jack',
        lastName: 'Black',
        username: 'Panda',
        memberNo: 34
      },
      {
        firstName: 'Joseph',
        lastName: 'Castillo',
        username: 'FrogZone',
        memberNo: 210
      }
      ];

    }
  ngOnInit(): void {
    setTimeout(() => {this.loadingMembers=true}, 5000);

  }
}

