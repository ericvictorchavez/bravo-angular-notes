import { Component } from '@angular/core';
import {User} from "../../models/User";

@Component({
  selector: 'app-user',
  templateUrl: './user.components.html',
  styleUrls: ['./user.components.css'],
  styles:['p{color: gold}']
})

export class UserComponent {
// Properties - like an attribute of the component
//   firstName = 'Clark';
  // lastName = 'Kent';
  // age = 65;

  // More practically way for creating properties
  // firstName: string;
  // lastName: string;
  // age: number;
  // whatever: any;
  // hasKids: boolean;

  //Data-Types
  // numberArray: number[];// this MUSt be a array of numbers
  //
  // stringArray: string[];// this MUSt be a array of strings
  //
  // mixArray: any[];// this can be a array of anything but it has to be an array

  //Might see this more often
  user: User;

//Methods-a "function" inside of a class

  //Constructor-runs every time when our components is init
  constructor() {
    // console.log('Hello from UserComponent!');
    //
    // this.greeting();
    // console.log(this.age);//undefined
    // this.hasBirthday();
    // console.log(this.age);//31
    // this.age =23
    // this.firstName= 'Clark';
    // this.lastName= 'Kents';
    // this.hasKids = false;
    // this.greeting();
    //
    // this.numberArray = [12, 33, 24];
    // this.stringArray = ['12', '33', '24'];
    // this.mixArray = [12, 'Eric', true];

    //Connecting with our interface
    this.user = {
      firstName: 'Jane',
      lastName: 'Doe',
      age: 30,
      email: 'jane@gmail.com',
    }
  }



  greeting() {
    console.log('Hello there, ' + this.user.firstName + ' ' + this.user.lastName);
  }

  hasBirthday(){
    this.user.age = 30;
    return this.user.age += 1;
  }



}

