import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-math',
  templateUrl: './math.component.html',
  styleUrls: ['./math.component.css']
})
export class MathComponent implements OnInit {

  num1: number;
  num2: number;
  constructor() {
    console.log(this.add(10,5));
    console.log(this.diff(10,5));
    this.num1= 10
    this.num2= 5
    // this.numThis();
    // console.log(this.num1 + this.num2);
    // console.log(this.num1 - this.num2);
    // console.log(this.num1 * this.num2);
    // console.log(this.num1 / this.num2);
    this.numSum();
    this.numDiff();
    this.numMult();
    this.numDiv();
    // this.table();

  }

  // numThis(){
  //   this.num1=10;
  //   this.num2=5;
  // }
   add(x,y){
    this.num1 = x;
    this.num2 = y;
    return this.num1 + this.num2;
   }
   diff
   (x,y) {
     this.num1 = x;
     this.num2 = y;
    return this.num1 - this.num2;
   }
   numSum(){
    console.log(this.num1 + '+' + this.num2 + '='+ (this.num1 + this.num2));
  }
  numDiff(){
    console.log(this.num1 + '-' + this.num2 + '='+ (this.num1 - this.num2));
  }
  numMult(){
    console.log(this.num1 + '*' + this.num2 + '='+ (this.num1 * this.num2));
  }
  numDiv(){
    console.log(this.num1 + '/' + this.num2 + '='+ (this.num1 / this.num2));

  }
  // table(){
  //   for (var i = 1; 1 < 100; i++){
  //     if (i % 3 === 0 && i % 15 !== 0){
  //       console.log('Fizz');
  //     } else if (i % 5 === 0 && i % 15 !== 0) {
  //       console.log('Buzz')
  //     }else if (i % 15 === 0) {
  //       console.log('FizzBuzz');
  //     }else {
  //       console.log(i);
  //     }
  //   }
  // }


  ngOnInit(): void {
  }

}
