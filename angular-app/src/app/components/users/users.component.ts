import { Component, OnInit, ViewChild } from '@angular/core';
import {User} from "../../models/User";
import {DataService} from "../../services/data.service";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  user: User= {
    firstName: "",
    lastName: "",
    age: null,
    email: "",
    image: "",
    balance: 0,
    memberSince: undefined,
    showUserForm: false,
  };
// Properties
  users: User[];
  displayInfo: boolean = false;
  showUserForm: boolean= false;
enableAddUser: boolean
currentClass: {}; //basically an empty object
currentStyle: {};
  @ViewChild("userForm")form: any
  //
data: any

// inject the service as a dependency in our constructor
  constructor(private dataService: DataService) {
  }

  ngOnInit(): void {
    this.dataService.getUsers().subscribe(users =>{
      this.users = users;
    });

    // this.users= this.dataService.getUsers();
    //subscribe to the observable to use getUsers() method...

    //subscribe to the observable
    // this.dataService.getData().subscribe(data =>{
    //   console.log(data)
    // });

  this.enableAddUser = true;

  this.setCurrentClasses()

    this.setCurrentStyle()


    //Calling our add user()
    // this.addUser({
    //   firstName: 'Jason',
    //   lastName: 'Todd',
    //   age: 13,
    // });

  }//End of NGONINIT

  onSubmit({value,valid}:{value:User, valid:boolean}) {
    if(!valid){
      alert("Your Form is invalid !!!!!!!!!!!!!")
    }else {
      value.isActive = true
      value.memberSince = new Date();
      value.hide = true;
    }
    //Faith's code from forms
      // this.users.unshift(value);

    //Stephen's code from services
    this.dataService.addUser(value);

    //resets the form
      this.form.reset();

  }

  // adding in fireEvent method under click event
  // fireEvent(e){
  //   console.log('The event has just happened');
  // //   // console.log(e.type);
  // //   console.log('The click event has occurred');
  // }

  toggleHide(user:User){
    user.hide = !user.hide;
  }


  // create a method that will change the state of our button
  setCurrentClasses() {
    this.currentClass = {
      'btn-dark': this.enableAddUser,
    }
  }
  //Create a method that will add padding top to the user's name
  // when the user's information is not displaying (this.displayInfo = false)
  setCurrentStyle(){
  this.currentStyle = {
    'padding-top': this.displayInfo ? '0' : '80px'
    }
  }

}//End of class

