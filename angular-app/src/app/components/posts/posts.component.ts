import { Component, OnInit } from '@angular/core';
import {Post} from "../../models/Post";
import {PostsService} from "../../services/posts.service";

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
//Properties
  posts: Post[];
  currentPost: Post = {
    id: 0,
    title: '',
    body: '',
  }
isEdit: boolean = false;
  //inject our service as a dependency
  constructor(private postsService: PostsService) {
  }

  //fetch the post when ngOnInit() is initialized
  ngOnInit(): void {
    // subscribe to our observable
    this.postsService.getPost().subscribe(post => {
      console.log('Fetching from our get...');
      this.posts = post;
    })

  }

  onNewPost(post: Post) {
    this.posts.unshift(post);
  }

  editPost(post: Post) {
    this.currentPost = post;
    // included for the update post
    this.isEdit = true;
  }

  onUpdatePost(post: Post) {
    this.posts.forEach((current, index) => {
      if (post.id === current.id) {
        //splicing the old post
        this.posts.splice(index, 1)

        this.posts.unshift(post);
        //resetting our button to 'Submit Post' btn
        this.isEdit = false
        //resetting our form
        this.currentPost = {
          id: 0,
          title:'',
          body:'',
      };
      }
    })
  }

  //create a method that will delete a single post
  deletePost(post: Post) {
    if (confirm('Are you sure')) {
      this.postsService.removePost(post.id).subscribe(() => {
        this.posts.forEach((current,index) =>{
          if(post.id === current.id) {
            this.posts.slice(index,1);

          }
        })
      })
    }
  }





}// end of class (don't delete)
