import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//Creating and Mapping Routes
import {HomeComponent} from "./components/home/home.component";
import {UsersComponent} from "./components/users/users.component";
import {PostsComponent} from "./components/posts/posts.component";
import {travelComponents} from "./components/travel/travel.components";
import {VipComponent} from "./components/vip/vip.component";

const routes: Routes = [
  {
    path: '', component: HomeComponent
  },
  {
    path: 'users', component: UsersComponent
  },
  {
    path:'posts',component: PostsComponent
  },
  {
    path:'travel',component: travelComponents
  },
  {
    path:'vip',component: VipComponent
  },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
