import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Post} from "../models/Post";

const httpOption = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
}

@Injectable({
  providedIn: 'root'
})

export class PostsService {
//Properties
  //set a url as a property
  postsURL: string = 'https://jsonplaceholder.typicode.com/posts';

  //inject the httpclient as a dependency
  constructor(private http: HttpClient) { }

  //create a method, that will make a GET request
  getPost() : Observable<Post[]> {
    return this.http.get<Post[]>(this.postsURL)
  }

  //create a method savePost()
  // savePost(post: Post): Observable<Post>{
  //   return this.http.post<Post>(this.postsURL, post, httpOption)
  // }

  savePost(post: Post) : Observable<Post> {
    return this.http.post<Post>(this.postsURL, post, httpOption);
  }
  updatePost(post: Post) : Observable<Post> {
    const url = `${this.postsURL}/${post.id}`;//url = https://jsonplaceholder.typicode.com/posts.id


    return this.http.put<Post>(url, post, httpOption);
  }
  removePost(post: Post | number) : Observable<Post> {
    const id = typeof post === 'number' ? post : post.id;
    const url = `${this.postsURL}/${id}`;

    console.log('Deleting post...');
    alert('Post removed');

    return this.http.delete<Post>(url, httpOption);
  }


}
