import { Injectable } from '@angular/core';
import {User} from "../models/User";
import {Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DataService {
  // Initialize a user's property and assign it to an array of User
  users: User[];

  data: Observable<any>;

  constructor() {
    this.users = [
      {
        firstName: 'Bruce',
        lastName: 'Wayne',
        age: 30,
        email: 'bruce@gmail.com',
        image: '/assets/img/bruce-wayne.jpg',
        balance: 1000000,
        memberSince: new Date('03/01/1937 08:30:00'),
        hide: true
      },
      {
        firstName: 'Diana',
        lastName: 'Prince',
        age: 110,
        email: 'diana@gmail.com',
        image: 'assets/img/diana-prince.jpg',
        isActive: true,
        balance: 1500000,
        memberSince: new Date('12/12/1942 12:30:00'),
        hide: true,
        showUserForm: false
      }
    ];//End of array}


  }//end of constructor



   //Types of events
  //(mousedown) (mouseup) (mouseout) (click) (dblclick) (drag)
  //getUsers(), return a type of User[]
  //refactor out method to return
  getUsers(): Observable<User[]> {
    console.log('Fetching users from service');
    return of(this.users);
  }
  // Create a method that adds a new user to the array
  addUser(user: User){
    console.log('Added user from service');
    this.users.unshift(user);
  }
  // getData(){
  //   this.data = new Observable(observer =>{
  //     setTimeout(() => {
  //       observer.next(1)
  //     }, 1000);
  //
  //     setTimeout(() => {
  //       observer.next(2)
  //     }, 2000);
  //
  //     setTimeout(() => {
  //       observer.next(3)
  //     }, 3000);
  //
  //     setTimeout(() => {
  //       observer.next(4)
  //     }, 4000);
  //     });
  //   return this.data;
  //   }
  }
