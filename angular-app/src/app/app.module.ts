import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//Components
import { AppComponent } from './app.component';
import { UserComponent} from "./components/user/user.component";
import { HelloComponents} from "./components/hello/hello.components";
import { travelComponents} from "./components/travel/travel.components";
import { MathComponent } from './components/math/math.component';
import { VipComponent } from './components/vip/vip.component';
import {UsersComponent} from "./components/users/users.component";
import { PostsComponent } from './components/posts/posts.component';
import { MaterialStylingComponent } from './components/material-styling/material-styling.component';
import { PostFormComponent } from './components/post-form/post-form.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';

//Services
import {DataService} from "./services/data.service";
import {PostsService} from "./services/posts.service";


@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    HelloComponents,
    travelComponents,
    MathComponent,
    VipComponent,
    UsersComponent,
    PostsComponent,
    PostFormComponent,
    HomeComponent,
    NavbarComponent,
    MaterialStylingComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatInputModule
  ],
  providers: [
    DataService,
    PostsService,

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

// Template Form
//
// import {Component} from '@angular/core';
// import {BrowserModule} from '@angular/platform-browser';
// import {NgModule} from "@angular/core";
// import {FormsModule} from "@angular/forms";
// import {AppComponent} from './app.component'
//
// @NgModule({
//   declarations:[AppComponent
//   ],
//   imports:[
//     BrowserModule,
//     FormsModule
//   ],
//   providers:[],
//   bootstrap:[AppComponent]
//
//
// })
// export class AppModule {
//
// }
//
// import {FormsModule, ReactiveFormsModule} from '@angular/forms';
// import {NgModule} from '@angular/core';
// import {BrowserModule} from '@angular/platform-browser';
// import { AppComponent } from './app.component';
// import { UsersComponent } from './components/users/users.component';
// @NgModule({
//   declarations: [// components go
//     AppComponent, UsersComponent
//   ],
//   imports: [ // modules go here
//     // other imports ...
//     ReactiveFormsModule,
//     BrowserModule,
//     FormsModule
//   ],
//   providers: [], // services go here
//   bootstrap: [AppComponent]
// })
// export class AppModule { }
