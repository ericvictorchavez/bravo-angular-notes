import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Bravo is';
}


//Template form
// import {Component} from '@angular/core';
// @Component({
//     selector: 'app-root',
//   templateUrl: './app.component.html',
//   styleUrls: ['./app.component.css']
//
// })
// @Component({
//   selector: 'app-root',
//   template: '<form #corona="ngForm" (ngSubmit)="onclickSubmit(corona.value)">\n' +
//     '  <input type="text" name="patient_name" placeholder="name" ngModel>\n' +
//     '  <br/>\n' +
//     '  <input type="text" name="patient_age" placeholder="age" ngModel>\n' +
//     '  <br/>\n' +
//     '  <input type="submit" value="submit">\n' +
//     '\n' +
//     '</form>\n',
//   styleUrls: ['./app.component.css']
//
// })
// export class AppComponent {
//   onclickSubmit(formData){
//     console.log('Corona infected patients is:' + formData.patient_name);
//     console.log('Corona infected patients age is:' + formData.patient_age);
//   }
// }
//
//  import {Component, OnInit} from '@angular/core';
//  import {FormGroup, FormControl} from "@angular/forms";
//
// @Component({
//     selector: 'app-root',
//   templateUrl: './app.component.html',
//   styleUrls: ['./app.component.css']
// })
//  export class AppComponent implements OnInit {
//   form: FormGroup;
//   ngOnInit() {
//     this.form = new FormGroup({
//       patient_name: new FormControl(''),
//       patient_age: new FormControl(''),
//     });
//   }
//   onclickSubmit(formData){
//      console.log('Corona infected patients is:' + formData.patient_name);
//      console.log('Corona infected patients age is:' + formData.patient_age);
//    }
// }
