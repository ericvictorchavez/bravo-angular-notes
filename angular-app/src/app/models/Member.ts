export interface Member {
  firstName?: string,
  lastName?: string,
  username?: string,
  memberNo?: number,
}
